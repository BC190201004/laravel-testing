<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->id();
            $table->string('studentId');
            $table->string('name');
            $table->unsignedBigInteger('creditHoursCompleted');
            $table->boolean('isEntryTestPassed');
            $table->unsignedBigInteger('CGPA');
            $table->string('academicStatus');
            $table->string('degreeProgram');
            $table->unsignedBigInteger('domainId');
            $table->string('registeredInSemester');
            $table->string('currentSemester');
            $table->string('createdOn');
            $table->string('inPhase');
            $table->string('phaseDueDate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
};
