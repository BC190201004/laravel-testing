<?php

namespace App\Imports;

use App\Http\Exports\StudentsExport;
use App\Models\student;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\ToArray;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class StudentsImport implements ToCollection, WithHeadingRow, WithValidation, FromArray
{

    protected $failed;

    public function collection(Collection $collection)
    {
        foreach ($collection as $row)
        {
            $this->failed .= $row;
        }
//        dd($this->failed);
//        $export = new StudentsExport();
//        dd($export);
    }

    public function rules(): array
    {
        return [

        ];
    }

    public function array(): array
    {
        // TODO: Implement array() method.
    }
}
