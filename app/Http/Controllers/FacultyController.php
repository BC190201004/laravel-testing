<?php

namespace App\Http\Controllers;

use App\Models\Faculty;
use App\Http\Requests\StoreFacultyRequest;
use App\Http\Requests\UpdateFacultyRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\Html\Button;


class FacultyController extends Controller
{

    public function loadData(Request $request, Builder $builder){
        if (request()->ajax()) {
//        dd('test');
            return DataTables::of(\App\Models\Faculty::query())
//                ->setRowClass(function ($faculty){
//                    return $faculty->id % 2 == 0 ? 'alert-success' : 'alert-warning';
//                })
                ->setRowId(function ($faculty){
                    return $faculty->id;
                })
                ->setRowAttr([
                    'align' => 'center'
                ])
                ->addColumn('action', '<button>kljsfd b</button>')
                ->editColumn('created_at', function($faculty){
                    return $faculty->created_at->diffForHumans();
                })
                ->editColumn('updated_at', function($faculty){
                    return $faculty->updated_at->diffForHumans();
                })
                ->make(true);
        }

        $html = $builder
            ->parameters([
                'searching' => true,
                'deferRender' => true,
                'scrollY' => true,
                'stateSave' => true,
                'pagingType' => 'full_numbers',
                'renderer' => 'bootstrap',
                'scrollCollapse' => true,
//                'search.search' => 'Fac'
                'fixedHeader.header' => true,
                'responsive' => true,
//                'scroller' => true,
//                'scrollY' => 450,
                'select' => true,
                'select.className' => 'alert alert-success',
                'select.blurable' => true,
//                'drawCallback' => 'function() { alert("Table Draw Callback") }',
//                'initComplete' => "function () {
//                            this.api().columns().every(function () {
//                                var column = this;
//                                var input = document.createElement(\"input\");
//                                $(input).appendTo($(column.footer()).empty())
//                                .on('keyup', function () {
//                                    column.search($(this).val(), false, false, true).draw();
//                                });
//                            });
//                        }",
            ])
            ->columns([
            ['data' => 'id', 'name' => 'id', 'title' => 'Sr. #'],
            ['data' => 'name', 'name' => 'name', 'title' => 'Name'],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => 'Created At'],
            ['data' => 'updated_at', 'name' => 'updated_at', 'title' => 'Updated At'],
            ['data' => 'action', 'name' => 'action', 'title' => 'Action', 'orderable' => false, 'printable' => false],
        ]);

        return view('htmlBuilder', compact('html'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse;
     */
    public function index()
    {
        return DataTables::of(Faculty::query())
            ->setRowClass(function ($faculty){
                return $faculty->id % 2 == 0 ? 'alert-success' : 'alert-warning';
            })
            ->setRowId(function ($faculty){
                return $faculty->id;
            })
            ->setRowAttr([
                'align' => 'center'
            ])
            ->addColumn('action', 'Action')
            ->editColumn('created_at', function($faculty){
                return $faculty->created_at->diffForHumans();
            })
            ->editColumn('updated_at', function($faculty){
                return $faculty->updated_at->diffForHumans();
            })
            ->make(true);
//        $faculties = Faculty::all();
//        return Datatables::of
//        return view('facultiesTest');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreFacultyRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreFacultyRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Faculty  $faculty
     * @return \Illuminate\Http\Response
     */
    public function show(Faculty $faculty)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Faculty  $faculty
     * @return \Illuminate\Http\Response
     */
    public function edit(Faculty $faculty)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateFacultyRequest  $request
     * @param  \App\Models\Faculty  $faculty
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateFacultyRequest $request, Faculty $faculty)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Faculty  $faculty
     * @return \Illuminate\Http\Response
     */
    public function destroy(Faculty $faculty)
    {
        //
    }
}
