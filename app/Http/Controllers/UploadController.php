<?php

namespace App\Http\Controllers;

use App\Imports\StudentsImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;


class UploadController extends Controller
{
    public function upload(Request $request){
        Excel::import(new StudentsImport, $request->file('file'));

//        return response()->json(['rows'=>$data]);
    }
}
