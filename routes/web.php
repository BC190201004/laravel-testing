<?php

use Illuminate\Support\Facades\Route;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Builder;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('upload', [\App\Http\Controllers\UploadController::class, 'upload']);
Route::view('upload', 'upload');

Route::resource('faculties', \App\Http\Controllers\FacultyController::class);

Route::view('test', 'facultiesTest');
Route::get('htmlbuilder', [\App\Http\Controllers\FacultyController::class, 'loadData']);

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
});
